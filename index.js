// Bài tập 1: Tính tiền lương nhân viên
function tinhLuong(){
    var luongMotNgayValue = document.getElementById("luongMotNgay").value*1;
    var soNgayValue = document.getElementById("soNgay").value*1;
    var luongThangValue = null;
    
    luongThangValue = luongMotNgayValue*soNgayValue;
    
    document.getElementById("kQB1").innerHTML = luongThangValue.toLocaleString() + " (VND)";
}

// Bài tập 2: Tính giá trị trung bình
function giaTriTrungBinh(){
    var num1Value = document.getElementById("num1").value*1;
    var num2Value = document.getElementById("num2").value*1;
    var num3Value = document.getElementById("num3").value*1;
    var num4Value = document.getElementById("num4").value*1;
    var num5Value = document.getElementById("num5").value*1;
    var giaTriTbValue = null;

    giaTriTbValue = (num1Value + num2Value + num3Value + num4Value +num5Value)/5;

    document.getElementById("kQB2").innerHTML = giaTriTbValue;
}

// Bài tập 3: Quy đổi tiền
function quyDoiTien(){
    var usdValue = document.getElementById("numUSD").value*1;
    const tyGia = 23500;

    var vndValue = usdValue*tyGia;

    document.getElementById("kQB3").innerHTML = vndValue.toLocaleString() + " (VND)";
}

// Bài tập 4: Tính diện tích, chu vi hình chữ nhật
function dTichChuVi(){
    var edge1Vallue = document.getElementById("edge1").value*1;
    var edge2Vallue = document.getElementById("edge2").value*1;
    var dTich = null;
    var cVi = null;

    dTich = edge1Vallue*edge2Vallue;
    cVi = (edge1Vallue + edge2Vallue)*2;

    document.getElementById("kQB4").innerHTML = "Diện tích = " + dTich + " , " + "Chu vi = " + cVi;
}

// Bài tập 5: Tính tổng 2 kí số
function tinhTong(){
    var numValue = document.getElementById("num").value*1;
    var soThuNhat = null;
    var soThuHai = null;
    var sum = null;

    soThuNhat = Math.floor(numValue/10);
    soThuHai = numValue%10;
    sum = soThuNhat + soThuHai;

    document.getElementById("kQB5").innerHTML = "Tổng 2 kí số = " + sum;
}